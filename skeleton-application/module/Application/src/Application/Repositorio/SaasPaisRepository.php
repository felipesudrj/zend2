<?php
namespace Application\Repositorio;

use Doctrine\ORM\EntityRepository;

use Application\Entidade\SaasPais;

/**
 * SaasPaisRepository
 */
class SaasPaisRepository extends EntityRepository
{
	const CLASS_PATH = 'Application\\Entidade\\SaasPais';

}