<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Repositorio\SaasPaisRepository;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {

    	$entityManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
    	$repositorio = $entityManager->getRepository(SaasPaisRepository::CLASS_PATH)->findAll();

        return new ViewModel();
    }

    public function cadastroAction()
    {
    	return new ViewModel();
    }
}
