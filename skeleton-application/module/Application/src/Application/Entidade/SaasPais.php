<?php
namespace Application\Entidade;

/**
 * SaasPais
 */
class SaasPais
{
    /**
     * Codigo do pais
     */
    private $cd_pais;

    /**
     * Nome do pais
     */
    private $ds_pais;

    /**
     * DS nacionalidade
     */
    private $ds_nacionalidade;

    /**
     * Gets the value of cd_pais.
     *
     * @return mixed
     */
    public function getCdPais()
    {
        return $this->cd_pais;
    }

    /**
     * Sets the value of cd_pais.
     *
     * @param mixed $cd_pais the cd pais
     *
     * @return self
     */
    public function setCdPais($cd_pais)
    {
        $this->cd_pais = $cd_pais;

        return $this;
    }

    /**
     * Gets the value of ds_pais.
     *
     * @return mixed
     */
    public function getDsPais()
    {
        return $this->ds_pais;
    }

    /**
     * Sets the value of ds_pais.
     *
     * @param mixed $ds_pais the ds pais
     *
     * @return self
     */
    public function setDsPais($ds_pais)
    {
        $this->ds_pais = $ds_pais;

        return $this;
    }
    /**
     * Gets the DS nacionalidade.
     *
     * @return mixed
     */
    public function getDsNacionalidade()
    {
        return $this->ds_nacionalidade;
    }

    /**
     * Sets the DS nacionalidade.
     *
     * @param mixed $ds_nacionalidade the ds nacionalidade
     *
     * @return self
     */
    public function setDsNacionalidade($ds_nacionalidade)
    {
        $this->ds_nacionalidade = $ds_nacionalidade;

        return $this;
    }

    /**
     * Desidrata
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            'cd_id' => $this->getCdPais(),
            'ds_descricao' => $this->getDsPais(),
            'cd_pais' => $this->getCdPais(),
            'ds_pais' => $this->getDsPais(),
            'ds_nacionalidade'=>$this->getDsNacionalidade(),
        );
    }


}
