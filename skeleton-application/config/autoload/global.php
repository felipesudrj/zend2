<?php
return array(
    'doctrine' => array(
        'connection' => array(
            // Conexão padrão MySQL
            'orm_default' => array(
                'driverClass' => 'Doctrine\DBAL\Driver\PDOMySql\Driver',
                'params' => array(
                    'host' => 'localhost',
                    'port' => '3306',
                    'user' => 'fullweb',
                    'password' => 'fullweb',
                    'dbname' => 'fullweb',
                    'driverOptions' => array(
                        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8'",
                    ),
                ),
            ),
        ),

    ),

    'phpSettings' => array(
        'date.timezone' => 'America/Sao_Paulo',
        'display_errors'=>true,
    ),
);